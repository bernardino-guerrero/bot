/*jshint esversion: 6 */

import APIAI from 'apiai';

export default class Bot {

	constructor(args){
		this.args = args;
		this.apiai = new APIAI(process.env.APIAI_KEY);
	}

	execute(){
      let text = this.args.text;
			console.log(this.args);
      return new Promise((resolve, reject)=>{
            let options = {
                  sessionId : this.args.sender
            };

            let request = this.apiai.textRequest(text, options);

            request.on("response", (response)=>{
                  let result = response.result;
                   resolve(result);
            });

           request.on('error', (err)=>{
              reject(err)
           });

           request.end();
      });
    }

}
